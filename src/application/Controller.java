package application;

import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

public class Controller implements Initializable{

	private static final int X_TILES = 14;
	private static final int Y_TILES = 14;
	
	private ArrayList<String> names = new ArrayList();
	String losingPlayer;
	TextInputDialog tid = new TextInputDialog("Geben Sie einen Namen ein");
	boolean p1 = false;
	
	@FXML
	GridPane grid;
	
	@FXML
	private Label playerName;
	
	tile[][] gameFields = new tile[X_TILES][Y_TILES];
	
	
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		int loop = 0;
		while(loop < 2) {
			TextInputDialog tid = new TextInputDialog();
			tid.setTitle("Add a player");
			tid.setHeaderText("");
			tid.setContentText("");
			Optional<String> name = tid.showAndWait();
			// Check if name != null and if the String is > 0
			if (name.isPresent() && name.get().length() > 0) {
					names.add(name.get());
					System.out.println("Player " + name.get() + " has been added.");
					
				
			} 
			loop++;
		}
		playerName.setText(names.get(0) + ": It's your turn.");
		
		grid.setStyle("-fx-grid-lines-visible : true");
		
		
		
		for (int x = 0; x < X_TILES;x++) {
			for (int y = 0; y < Y_TILES; y++) {
				tile temp = new tile(this);
				grid.add(temp.getTile(), x, y);
				gameFields[x][y] = temp;
			}
		}
		
		
		
	}
	
	
	
	
	
	private class tile extends StackPane{
		boolean hasBomb = false;
		private Image bomb = new Image ("file:./GUIAssets/kreis.png",50	,50,false,false);
		private Image empty = new Image ("file:./GUIAssets/basic.png",50,50,false,false);
		Rectangle fg = new Rectangle (50,50);
		StackPane tile = new StackPane();
		private Text value = new Text();
		ImageView IVBomb = new ImageView();
		ImageView IVEmpty = new ImageView();
		Controller controller;
		
		
		public tile (Controller controller) {
			fg.setStroke(Color.WHITE);
			this.controller = controller;
			double random = Math.random();
		
			if (random < 0.2) {
				IVBomb.setImage(bomb);
				hasBomb = true;
				tile.getChildren().addAll(IVBomb,fg);
			}
			else  {
				IVEmpty.setImage(empty);
				random = Math.random() * 10;
				int i = (int)random;
				value.setText(String.valueOf(i));
				tile.getChildren().addAll(IVEmpty,value,fg);
				
				
			}
			
		fg.setOnMouseClicked(e ->{
			fg.setVisible(false);
			if (hasBomb == true) {
				controller.showAllBombs();
			} else {
				if (controller.p1 == true) {
					controller.playerName.setText(controller.names.get(0) + ": It's your turn.");
					controller.losingPlayer = controller.names.get(0);
					controller.p1 = false;
				} else {
					controller.playerName.setText(controller.names.get(1) + ": It's your turn.");
					controller.losingPlayer = controller.names.get(1);
					controller.p1 = true;
				}
			}
		});
			
		}
		
		
		public StackPane getTile() {
			return tile;
		}
		
		
	}
	
	
	public void showAllBombs() {
		for (int x = 0; x < X_TILES; x++) {
			for (int y = 0; y < Y_TILES; y++) {
				if (gameFields[x][y].hasBomb == true) {
					gameFields[x][y].fg.setVisible(false);
					
				}
			}
		}
		Alert lost = new Alert(AlertType.INFORMATION);
		lost.setContentText(losingPlayer + "! YouLost!");
		lost.showAndWait();
	}

}
